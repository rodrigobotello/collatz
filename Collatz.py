#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C) 2016
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------


# this is the chache
cache = {}

# this is a helper function


def help_collatz(n):
    # set n equal to number
    number = n
    assert n > 0
    # if the value already in the cache
    if n in cache:
        return cache[n]
    c = 1
    while n > 1:
        if (n % 2) == 0:
            n = (n // 2)
        else:
            n = (3 * n) + 1
        c += 1
    assert c > 0
    # save in the cache
    cache[number] = c
    return c


def collatz_eval(i, j):
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # num will be the biggest number
    num = 1
    if i - j == 0:
        num = help_collatz(i)
    else:
        for iterate in range(min(i, j), max(i, j) + 1):
            # use the helper function to obtain the value
            col = help_collatz(iterate)
            # if the number obtained from the helper funciton is greater than the current
            if col > num:
                num = col
    return num

# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
